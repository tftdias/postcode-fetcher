package com.tftdias.labs.client;

import com.tftdias.labs.exceptions.InvalidPostcodeException;
import com.tftdias.labs.exceptions.PostcodesClientException;
import com.tftdias.labs.model.Response.NearestResponse;
import com.tftdias.labs.model.Postcode;
import com.tftdias.labs.model.Response.PostcodeResponse;
import com.tftdias.labs.model.Response.ValidateResponse;
import com.tftdias.labs.utils.RequestExecutor;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class PostcodesRESTClient implements PostcodesClient {

    private static final String POSTCODES_API_URL = "http://postcodes.io/postcodes";

    private Client client;

    private RequestExecutor executor;


    public PostcodesRESTClient() {

        this.client = ClientBuilder.newBuilder()
                                   .connectTimeout(5, TimeUnit.SECONDS)
                                   .readTimeout(5, TimeUnit.SECONDS)
                                   .build();

        this.executor = new RequestExecutor(client,
                                            POSTCODES_API_URL,
                                            MediaType.APPLICATION_JSON);
    }

    @Override
    public void validate(String postcode) throws InvalidPostcodeException, PostcodesClientException {

        try {
            boolean isValid = this.executor.doRequest(postcode + "/validate", ValidateResponse.class)
                    .getResult();

            if (!isValid) {
                throw new InvalidPostcodeException(postcode);
            }
        } catch (ProcessingException pe) {
            throw handleProcessingException(pe);
        }
    }

    @Override
    public Postcode get(String postcode) throws PostcodesClientException {

        try {

            return this.executor.doRequest(postcode, PostcodeResponse.class)
                                .getResult();

        } catch (NotFoundException nfe) {
            throw new PostcodesClientException("No info available for postcode " + postcode);
        } catch (ProcessingException pe) {
            throw handleProcessingException(pe);
        }
    }

    @Override
    public List<Postcode> getNearest(String postcode) throws PostcodesClientException {

        try {

            return this.executor.doRequest(postcode + "/nearest", NearestResponse.class)
                                .getResult();

        } catch (NotFoundException nfe) {
            throw new PostcodesClientException("No nearest info available for postcode " + postcode);
        } catch (ProcessingException pe) {
            throw handleProcessingException(pe);
        }
    }

    @Override
    public void close() {
        this.client.close();
    }

    private PostcodesClientException handleProcessingException(ProcessingException pe) {
        if (pe.getCause() instanceof TimeoutException) {
            return new PostcodesClientException("Operation timed out");
        } else {
            return new PostcodesClientException();
        }
    }

}
