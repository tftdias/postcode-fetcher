package com.tftdias.labs.client;

import com.tftdias.labs.exceptions.InvalidPostcodeException;
import com.tftdias.labs.exceptions.PostcodesClientException;
import com.tftdias.labs.model.Postcode;

import java.util.List;


public interface PostcodesClient {

    void validate(String postcode) throws InvalidPostcodeException, PostcodesClientException;

    Postcode get(String postcode) throws PostcodesClientException;

    List<Postcode> getNearest(String postcode) throws PostcodesClientException;

    void close();

}