package com.tftdias.labs.exceptions;

public class PostcodesClientException extends Exception {

    private static final String baseMessage = "Unable to fulfill request";

    public PostcodesClientException() {
        super(baseMessage);
    }

    public PostcodesClientException(String message) {
        super(String.format("%s: %s", baseMessage, message));
    }

}
