package com.tftdias.labs.exceptions;

public class InvalidPostcodeException extends Exception {

    public InvalidPostcodeException() {
        super();
    }

    public InvalidPostcodeException(String postcode) {
        super(String.format("ERROR: Postcode %s is not valid.", postcode));
    }

}
