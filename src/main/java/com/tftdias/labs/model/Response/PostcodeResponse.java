package com.tftdias.labs.model.Response;

import com.tftdias.labs.model.Postcode;

public class PostcodeResponse extends PostcodeAPIResponse<Postcode> {

    public PostcodeResponse() { }

    public PostcodeResponse(Integer status, Postcode result) {
        super();

        this.status = status;
        this.result = result;
    }

}
