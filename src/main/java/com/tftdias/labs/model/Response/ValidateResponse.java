package com.tftdias.labs.model.Response;

public class ValidateResponse extends PostcodeAPIResponse<Boolean> {

    public ValidateResponse() { }

    public ValidateResponse(Integer status, Boolean result) {
        super();

        this.status = status;
        this.result = result;
    }

}
