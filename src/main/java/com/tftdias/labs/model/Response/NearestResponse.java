package com.tftdias.labs.model.Response;

import com.tftdias.labs.model.Postcode;

import java.util.List;

public class NearestResponse extends PostcodeAPIResponse<List<Postcode>> {

    public NearestResponse() { }

    public NearestResponse(Integer status, List<Postcode> result) {
        super();

        this.status = status;
        this.result = result;
    }
}
