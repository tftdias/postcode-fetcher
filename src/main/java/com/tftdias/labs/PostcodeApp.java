package com.tftdias.labs;

import com.tftdias.labs.client.PostcodesClient;
import com.tftdias.labs.client.PostcodesRESTClient;
import com.tftdias.labs.exceptions.InvalidPostcodeException;
import com.tftdias.labs.exceptions.PostcodesClientException;
import com.tftdias.labs.model.Postcode;

import java.util.List;

public class PostcodeApp {

    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println("Usage: java PostcodeApp [postcode]");
            System.exit(0);
        }

        String postcodeId = args[0].trim();

        PostcodesClient client = new PostcodesRESTClient();

        try {

            client.validate(postcodeId);

            System.out.println(String.format("Postcode %s is valid. Fetching info...\n", postcodeId));

            Postcode postcode = client.get(postcodeId);
            System.out.println(String.format("Country: %s\nRegion: %s", postcode.getCountry(), postcode.getRegion()));

            List<Postcode> nearest = client.getNearest(postcodeId);

            StringBuilder builder = new StringBuilder("Nearest:\n");

            if (nearest.isEmpty()) {
                builder.append("no postcodes available");
            } else {
                nearest.forEach((pc) -> builder.append("\t").append(pc.getId()).append("\n"));
            }

            System.out.println(builder.toString());

        } catch (InvalidPostcodeException|PostcodesClientException e) {

            System.out.println(e.getMessage());

        } finally {

            client.close();

        }
    }
}
