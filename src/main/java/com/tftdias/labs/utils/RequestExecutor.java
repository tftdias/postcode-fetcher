package com.tftdias.labs.utils;

import javax.ws.rs.client.Client;

public class RequestExecutor {

    private Client client;

    private String baseUrl;

    private String[] acceptedTypes;

    public RequestExecutor(Client client, String baseUrl, String... acceptedTypes) {

        this.client = client;
        this.baseUrl = baseUrl;
        this.acceptedTypes = acceptedTypes;
    }

    public <T> T doRequest(String path, Class<T> responseType) {

        return client.target(this.baseUrl)
                .path(path)
                .request(this.acceptedTypes)
                .get(responseType);
    }

}
