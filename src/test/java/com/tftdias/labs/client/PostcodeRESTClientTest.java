package com.tftdias.labs.client;

import com.tftdias.labs.exceptions.InvalidPostcodeException;
import com.tftdias.labs.exceptions.PostcodesClientException;
import com.tftdias.labs.model.Postcode;
import com.tftdias.labs.model.Response.NearestResponse;
import com.tftdias.labs.model.Response.PostcodeResponse;
import com.tftdias.labs.model.Response.ValidateResponse;
import com.tftdias.labs.utils.RequestExecutor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;


class PostcodeRESTClientTest {

    private static final String validPostcode = "CB3 0FA";
    private static final String validPostcodeCountry = "England";
    private static final String validPostcodeRegion = "East of England";
    private static final List<Postcode> validPostcodeNearest = Arrays.asList(
            new Postcode("CB3 0FA", "England", "East of England"),
            new Postcode("CB3 0GT", "England", "East of England"),
            new Postcode("CB3 0FT", "England", "East of England")
    );

    private static final String invalidPostcode = "abcdefghi";

    @Mock(name = "client")
    Client internalClient;

    @Mock
    RequestExecutor executor;

    @InjectMocks
    private PostcodesRESTClient client;

    @BeforeEach
    private void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void validatePostcode_withValidPostcode_isOk() throws InvalidPostcodeException, PostcodesClientException {
        given(this.executor.doRequest(validPostcode + "/validate", ValidateResponse.class))
                .willReturn(new ValidateResponse(200, true));

        this.client.validate(validPostcode);
    }

    @Test
    void validatePostcode_givenInvalidPostcode_throwsException() {
        given(this.executor.doRequest(invalidPostcode + "/validate", ValidateResponse.class))
                .willReturn(new ValidateResponse(200, false));

        Throwable thrown = catchThrowable(() -> this.client.validate(invalidPostcode));

        assertThat(thrown).isInstanceOf(InvalidPostcodeException.class);
    }

    @Test
    void validatePostcode_givenOperationTimeout_throwsException() {
        given(this.executor.doRequest(validPostcode + "/validate", ValidateResponse.class))
                .willThrow(ProcessingException.class);

        Throwable thrown = catchThrowable(() -> this.client.validate(validPostcode));

        assertThat(thrown).isInstanceOf(PostcodesClientException.class);
    }

    @Test
    void getPostcode_givenValidPostcode_returnsPostcodeInfo() throws PostcodesClientException {
        given(this.executor.doRequest(validPostcode, PostcodeResponse.class))
                .willReturn(new PostcodeResponse(200, new Postcode(validPostcode, validPostcodeCountry, validPostcodeRegion)));

        Postcode result = this.client.get(validPostcode);

        assertEquals(validPostcode, result.getId());
        assertEquals(validPostcodeCountry, result.getCountry());
        assertEquals(validPostcodeRegion, result.getRegion());
    }

    @Test
    void getPostcode_givenInvalidPostcode_throwsException() {
        given(this.executor.doRequest(invalidPostcode, PostcodeResponse.class))
                .willThrow(NotFoundException.class);

        Throwable thrown = catchThrowable(() -> this.client.get(invalidPostcode));

        assertThat(thrown).isInstanceOf(PostcodesClientException.class);
    }

    @Test
    void getNearest_givenValidPostcode_returnListWithNearest() throws PostcodesClientException {
        given(this.executor.doRequest(validPostcode + "/nearest", NearestResponse.class))
                .willReturn(new NearestResponse(200, validPostcodeNearest));

        List<Postcode> result = this.client.getNearest(validPostcode);

        assertEquals(validPostcodeNearest, result);
    }

    @Test
    void getNearest_givenInvalidPostcode_throwsException() {
        given(this.executor.doRequest(invalidPostcode + "/nearest", NearestResponse.class))
                .willThrow(NotFoundException.class);

        Throwable thrown = catchThrowable(() -> this.client.getNearest(invalidPostcode));

        assertThat(thrown).isInstanceOf(PostcodesClientException.class);
    }
}
